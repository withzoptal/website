import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import {
  CanActivateRouteGuard as AuthGuard
} from './configs/can-activate-route.guard';

import { HomeComponent } from './view/home/home.component';
import { AboutComponent } from './view/about/about.component';
import { ContactComponent } from './view/contact/contact.component';
import { PagenotfoundComponent } from './view/pagenotfound/pagenotfound.component';
import { HeaderComponent } from './view/header/header.component';
import { FooterComponent } from './view/footer/footer.component';
import { MainSectionComponent } from './view/main-section/main-section.component';
import { FavoriteRestaurantsComponent } from './view/favorite-restaurants/favorite-restaurants.component';
import { NeighbourhoodRestaurantsComponent } from './view/neighbourhood-restaurants/neighbourhood-restaurants.component';
import { UserDirectoryComponent } from './view/user-directory/user-directory.component';
import { TermsComponent } from './view/terms/terms.component';
import { PrivacyPolicyComponent } from './view/privacy-policy/privacy-policy.component';
import { DetailComponent } from './view/neighbourhood-restaurants/detail/detail.component';
import { MyAccountComponent } from './view/my-account/my-account.component';
import { ChangePasswordComponent } from './view/change-password/change-password.component';
import { PromotionComponent } from './view/promotion/promotion.component';
import { ConfirmComponent } from './view/confirm/confirm.component';
import { AddRestaurantComponent } from './view/add-restaurant/add-restaurant.component';
import { EditProfileComponent } from './view/edit-profile/edit-profile.component';
import { FollowComponent } from './view/follow/follow.component';
import { ViewProfileComponent } from './view/view-profile/view-profile.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'  },
  {
    path: '', component: MainSectionComponent,
    children: [
        { path: 'home', component: HomeComponent },
        { path: 'about-us', component: AboutComponent },
        { path: 'contact', component: ContactComponent },
        { path: 'add-restaurant', component: AddRestaurantComponent, canActivate: [AuthGuard] },
        { path: 'favorite-restaurants', component: FavoriteRestaurantsComponent, canActivate: [AuthGuard] },
        { path: 'favorite-restaurants/:id', component: DetailComponent, canActivate: [AuthGuard] },
        { path: 'neighbourhood-restaurants', component: NeighbourhoodRestaurantsComponent },
        { path: 'neighbourhood-restaurants/:id', component: NeighbourhoodRestaurantsComponent },
        { path: 'neighbourhood-restaurants-detail', component: DetailComponent },
        { path: 'my-account', component: MyAccountComponent, canActivate: [AuthGuard] },
        { path: 'view-profile/:id', component: ViewProfileComponent, canActivate: [AuthGuard] },
        { path: 'follow', component: FollowComponent, canActivate: [AuthGuard] },
        { path: 'edit-account', component: EditProfileComponent, canActivate: [AuthGuard] },
        { path: 'change-password', component: ChangePasswordComponent, canActivate: [AuthGuard] },
        { path: 'promotion', component: PromotionComponent, canActivate: [AuthGuard] },
        { path: 'user-directory', component: UserDirectoryComponent, canActivate: [AuthGuard] },
        { path: 'terms', component: TermsComponent },
        { path: 'privacy-policy', component: PrivacyPolicyComponent },
        //{ path: 'page-not-found', component: PagenotfoundComponent },
        
      ],
    },
    { path: '**', component: PagenotfoundComponent },
  
  // { path: '', redirectTo: '', pathMatch: 'full'  },
  // { path: 'home', component: HomeComponent },
  // { path: 'contact', component: ContactComponent },
  // { path: 'about', component: AboutComponent },
  // { path: 'favorite-restaurants', component: FavoriteRestaurantsComponent },
  // { path: '**', component: PagenotfoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false, useHash: true, anchorScrolling: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
