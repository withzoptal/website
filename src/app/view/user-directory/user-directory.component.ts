import { Component, OnInit, AfterViewInit  } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import { ApiCallingService } from "../../configs/api-calling.service"
import { NotifierService } from "angular-notifier";
import * as $ from 'jquery';


@Component({
  selector: 'app-user-directory',
  templateUrl: './user-directory.component.html',
  styleUrls: ['./user-directory.component.css']
})
export class UserDirectoryComponent implements OnInit {
  private readonly notifier: NotifierService;
  public userDirectory : any = [];
  public IMAGE_BASE_URL;
  public limit = '';
  public dictionary: any = [];
  public alphabets = ['A', 'B', 'C','D', 'E', 'F','G', 'H', 'I','J', 'K', 'L','M', 'N', 'O','P', 'Q', 'R','S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
 
  // pagination var
  itemsPerPage: number = 10;
  totalRecords: number;
  skip: number = 0;
  p: number = 1;
  private stage  = '';
  total ='';

  constructor(
    private http: HttpClient,
    private router: Router,
    public api: ApiCallingService,
    notifierService: NotifierService,
    ) { this.notifier = notifierService; }

  ngOnInit() {

    this.userdirectory();
    this.api.onActivateScroll();
    this.IMAGE_BASE_URL = this.api.IMAGE_BASE_URL;
   
  }
  
  getUsers(text){
        $("#search-bar").val("");
        var api_url = 'Profils/searchUsersToFollow?access_token='+localStorage.getItem('token')+'&offset=0&query='+text.toLowerCase().trim()
          this.api.getRequest(api_url, {}).then( 
            (res) => {
                this.userDirectory = res;
            },
            (err) => {
            }
          );

  }
  searchUsers(){

    var api_url = 'Profils/searchUsersToFollow?access_token='+localStorage.getItem('token')+'&offset=0&query='+$("#search-bar").val();
    this.api.getRequest(api_url, {}).then( 
      (res) => {
          this.userDirectory = res;
      },
      (err) => {
      }
    );

  }

  userdirectory(){

    var api_url = 'Profils?access_token='+localStorage.getItem('token')
    this.api.getRequest(api_url, {}).then( 
      (res) => {
          for(var i in res){
            if(res[i].role=="client"){
              this.userDirectory.push(res[i]);
            }
          }
      },
      (err) => {
      }
    );
  }
 

}