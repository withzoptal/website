import { Component, OnInit, ViewChild, ElementRef, OnDestroy} from '@angular/core';
import { TabsetComponent, TabDirective } from 'ngx-bootstrap/tabs';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';

import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiCallingService } from "../../configs/api-calling.service"
import { NotifierService } from "angular-notifier";
import * as $ from 'jquery';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AutoCompleteComponent, ChangeEventArgs, FilterType} from '@syncfusion/ej2-angular-dropdowns';
import { Content } from '@angular/compiler/src/render3/r3_ast';

@Component({
  selector: 'app-add-restaurant',
  templateUrl: './add-restaurant.component.html',
  styleUrls: ['./add-restaurant.component.css']
})
export class AddRestaurantComponent implements OnInit {
  searchForm: FormGroup;
  form: FormGroup;
  @ViewChild('tabset', {read: ElementRef}) tabsetEl: ElementRef;
  @ViewChild('sample')
  public autoCompleteObj: AutoCompleteComponent;
  modalReference: NgbModalRef;
  public fields: Object = { value: 'Name' };
  private readonly notifier: NotifierService;
  public userDetail: any = {};
  public responseData: any = [];
  public IMAGE_BASE_URL;
  public restaurants: any= [];
  submitted = false;
  restaurantId= '';
  closeResult = '';
  restaurant_name= '';
  restaurant_addr = '';
  restaurant_photo='';
  public favResturants: any= [];
  public qualities_array : any =[];
  public restaurantData: any = [];
  public placeData: any = [];
  public qualityData: any = [];

  // pagination var
  itemsPerPage: number = 10;
  totalRecords: number;
  skip: number = 0;
  p: number = 1;
  private stage  = '';
  total ='';
  
   
  constructor(
    private modalService: NgbModal,
    private http: HttpClient,
    private router: Router,
    public api: ApiCallingService,
    notifierService: NotifierService,
    private formBuilder: FormBuilder,
  ) { 
    this.notifier = notifierService;
    this.userDetail = JSON.parse(localStorage.getItem('userDetails'));
  }

  ngOnInit() {
    this.searchForm = this.formBuilder.group({
      search: ['', [Validators.required, Validators.minLength(1)]],
      avatar: [null],
    });
    this.IMAGE_BASE_URL = this.api.IMAGE_BASE_URL;
    this.form = new FormGroup({
      services: new FormControl(null)
   });
  
  }
  get f() { return this.searchForm.controls; }

  get fg() { return this.form.controls; }

  searchRestaurant(){
   
    this.submitted = true;
    // stop here if form is invalid 
    if (this.searchForm.invalid) { 
      return;
    } else {
     
      let api_url = 'Restaurants/search?access_token=' + localStorage.getItem('token')+'&offset=0';
      if (this.searchForm.controls.search) {
        api_url = api_url + '&query=' + this.searchForm.controls.search.value + '&location='+ localStorage.getItem('latlang');
      }
      console.log("v++++++++++PPP", api_url)
      //$("#restaurant_1").DataTable().clear(); 
      this.api.getRequest(api_url, {}).then(
        (res) => {  

          console.log("v++++++++++PPP", res)
          
          if(res!= undefined && res.length>0){
            this.restaurants = [];
          
              for(var i in res){
                res[i].photo && res[i].photo!='' ? res[i].photo : '../../../assets/img/no-photo.png';
                
                if(res[i].rating){
                  const ratings = res[i].rating;
                  const starTotal = 5;
                  const starPercentage = (ratings / starTotal) * 100;
                  res[i].starPercentageRounded = `${(Math.round(starPercentage / 10) * 10)}%`;
                }else{
                  res[i].starPercentageRounded =  '0%';
                }
                if(res[i].reviews){
                  res[i].reviewcount = res[i].reviews.length

                }else{
                  res[i].reviewcount =  '0';
                }

                this.restaurants.push(res[i]);
              }
          }else{
            this.notifier.notify('error', `Error : ${'Restaurants not found.'}`);

          }
        },
        (err) => {

        }
      ); 

    }
  }
 
  // open(content, restaurant_id) {
  //   var data = this.getRestaurants(restaurant_id);
  //   console.log("+++++data++++++++", data)

  //   this.MESSAGE_DATA = "PASS DATA TO ANGULAR2 MODAL"
  //   this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'}).result.then((result) => {
  //     this.closeResult = `Closed with: ${result}`;
  //   }, (reason) => {
  //     this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  //   });
  // }  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  getRestaurants(content, restaurant_id) {

    let api_url = 'Restaurants/GetDetails?access_token='+localStorage.getItem('token');
    api_url = api_url + '&placeId=' + restaurant_id;

    this.api.getRequest(api_url, { placeId: restaurant_id }).then(
      (res) => {

        this.restaurant_name = res.name;
        this.restaurant_addr = res.formatted_address
        this.restaurant_photo= res.photo;
        this.restaurantId= res.id;
      

         this.api.getRequest('Qualities', {}).then(
          (qualities) => { 
              if(qualities){
                this.qualities_array = [];
                for(var i in qualities){
                  if(qualities[i].name){
                    this.qualities_array.push(qualities[i]);
                  }
                }
                this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg', centered: true, }).result.then((result) => {
                  this.closeResult = `Closed with: ${result}`;
                }, (reason) => {
                  this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
                });
               
              }
          },
          (err) => {

          }
        );
      },
      (err) => {

      }
    );

  }

  addInFavoritesList(restaurantId){

    this.submitted = true;
    // stop here if form is invalid 
    if (this.form.invalid) {
      return;
    } else {
      let api_url = 'Restaurants/addFavRest?access_token='+localStorage.getItem('token');
      let requestData = {
        "restaurantId": restaurantId,
        "qualitie": this.favResturants,
      }
      if(this.favResturants.length >= 3){

        Swal.fire({
          title: 'How expensive is this restaurant?',
          input: 'radio',
          inputOptions: {
            '#ff0000': 'Cheap',
            '#00ff00': 'Moderate',
            '#0000ff': 'Expensive',
            '#00FF00': 'very Expensive'
          },
          inputValidator: function(result) {
            if (!result) {
              return 'You need to select something!';
            }
          }
        }).then((result) => { 
          
          if (result.value) {
            this.api.postRequest(api_url,requestData).then( 
            (res) => {   
              Swal.fire(
                  'Restaurant!',
                  'Added successfully in your favourite list.',
                  'success'
                )
                //this.getDismissReason('content')
                $(".btn-outline-dark").click();
                //this.router.navigate(['/my-account']);
            },
            (err) => {
              Swal.fire(
                'Restaurant!',
                'Please provide valid data.',
                'error'
              )
            }
          );
          } else if (result.value === 0) {
            Swal.fire({icon: 'error', text: "Please choose one of these :("});
        
          } else {
            console.log(`modal was dismissed by ${result.dismiss}`)
          }
        });


         
      }else{
        Swal.fire(
          'Restaurant!',
          'Please provide minimum 3 quality.',
          'error'
        )

      }


    }
  
  }
  sendit(event:any){

    let api_url = 'Restaurants/searchAutocomplete?query='+event.target.value+'&access_token='+localStorage.getItem('token')+'&location=';
    this.api.getRequest(api_url,{}).then( 
      (res) => {
       
        for(var i in res){
         var obj =  { Name: res[i].name, Address:  res[i].formatted_address}
        
          this.placeData.push(obj);
        }
        this.restaurantData=  this.placeData;

      },
      (err) => {
       
      }
    );

  }

  remove_duplicates(arr) {
    let obj = {};
    for (let i = 0; i < arr.length; i++) {
        obj[arr[i]] = true;
    }
    arr = [];
    for (let key in obj) {
        arr.push(key);
    }
    return arr;
  }




  favRest(id){
    if (this.favResturants.includes(id)) {
      this.favResturants = this.favResturants.slice(0, this.favResturants.indexOf(id))

    } else {
      this.favResturants.push(id)
    }
  }

}
