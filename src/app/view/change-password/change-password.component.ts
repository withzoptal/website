import { Component, OnInit, Input } from '@angular/core';
//import { NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { NotifierService } from "angular-notifier";
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiCallingService } from "../../configs/api-calling.service"
import Swal from 'sweetalert2/dist/sweetalert2.js'

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  @Input() name;
  changePasswordForm: FormGroup;
  submitted = false;
  requestData: any;
  private readonly notifier: NotifierService;
  public userDetail: any = {};

  constructor(
    //public activeModal: NgbActiveModal, 
    //private modalService: NgbModal,
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private router: Router,
    public api: ApiCallingService,
    notifierService: NotifierService,
    ) { 
      this.notifier = notifierService;
      this.userDetail = JSON.parse(localStorage.getItem('userDetails'));
    }

  ngOnInit() {
   
    this.changePasswordForm = this.formBuilder.group({
      currentpassword: ['', [Validators.required, Validators.minLength(4)]],
      password: ['', [Validators.required, Validators.minLength(4)]],
      verifypassword: ['',[Validators.required, Validators.minLength(4)]],
    });
    this.api.onActivateScroll();

  }
  get f() { return this.changePasswordForm.controls; }

  changePassword(){

    this.submitted = true;
    // stop here if form is invalid 
    if (this.changePasswordForm.invalid) {
      return;
    } else {

      let api_url = 'Profils/change-password?access_token='+localStorage.getItem('token')

      let requestData = {
        "oldPassword": this.changePasswordForm.controls.currentpassword.value,
        "newPassword": this.changePasswordForm.controls.verifypassword.value,
      }

      this.api.postRequest(api_url,requestData).then( 
        (res) => {
           Swal.fire(
              'Password!',
              'Your new password has been set successfully.',
              'success'
            )
            this.router.navigate(['/my-account']);
        },
        (err) => {
          Swal.fire(
            'Password!',
            'Please provide valid data.',
            'error'
          )
        }
      );


    }

  }
  

}
