import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from '../login/login.component';
import * as $ from 'jquery';
// import { SignupComponent } from '../signup/signup.component';
// import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  userToken = {};
  show: boolean = true;
  account: boolean = true;
  scroll:boolean=false;
  constructor( private modalService: NgbModal) { }

  ngOnInit() {
    //window.addEventListener('scroll', this.scrolling, true)
    this.userToken = localStorage.getItem('token');
    if(this.userToken == null || this.userToken == '' ){
    this.show = true;
    this.account = false;

    }else{
      this.show = false;
      this.account = true;
    }

    $(".footer-links").on('click', function(){
     
      $("html, body").animate({ scrollTop: 0 }, "slow");

    })

  }

  openLogin() {
    this.modalService.open(LoginComponent);
  }


}
