import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiCallingService } from "../../configs/api-calling.service"
import { NotifierService } from "angular-notifier";
import Swal from 'sweetalert2/dist/sweetalert2.js'


@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.css']
})
export class PrivacyPolicyComponent implements OnInit {
  editForm: FormGroup;
  submitted = false;
  requestData: any;
  public userDetail: any = {};
  fileToUpload: File = null;
  public fileUrl: any = '';
  private readonly notifier: NotifierService;
  public IMAGE_BASE_URL;
  public privacy_policy;
 
  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private router: Router,
    public api: ApiCallingService,
    notifierService: NotifierService
  ) { 
    this.notifier = notifierService;
    this.userDetail = JSON.parse(localStorage.getItem('userDetails')); }

  ngOnInit() {
    this.IMAGE_BASE_URL = this.api.IMAGE_BASE_URL;
    this.privacy_policy = this.IMAGE_BASE_URL+"Containers/grumpeat/download/1538555386283_grumpeat_Privacy%20Policy.pdf";
    this.api.onActivateScroll();
    this.getPrivacy();

  }

  getPrivacy(){

    let api_url ='Containers/grumpeat/download/1538555386283_grumpeat_Privacy%20Policy.pdf'

  }

}
