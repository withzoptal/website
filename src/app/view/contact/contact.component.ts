import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiCallingService } from "../../configs/api-calling.service"
import { NotifierService } from "angular-notifier";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  contactForm: FormGroup;
  submitted = false;
  requestData: any;
  subject= {};
  private readonly notifier: NotifierService;
  
  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private router: Router,
    public api: ApiCallingService,
    notifierService: NotifierService,
    ) { this.notifier = notifierService;}

  ngOnInit() {
    this.contactForm = this.formBuilder.group({
        name: ['', [Validators.required]],
        email: ['', [Validators.required, Validators.compose([
          Validators.required,Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ])]],
        phone: ['', [Validators.required]],
        subject: ['', [Validators.required]],
        message: ['', [Validators.required]],
    });
  }
  get f() { return this.contactForm.controls; }

  contact(){
    this.submitted = true;
    //stop here if form is invalid 
    if (this.contactForm.invalid) {
      return;
    } else {

      let api_url = 'User/contact'
      
      let requestData = {
        "name": this.contactForm.controls.name.value,
        "email": this.contactForm.controls.email.value,
        "phone": this.contactForm.controls.phone.value,
        "subject": this.contactForm.controls.subject.value,
        "message": this.contactForm.controls.message.value,
      }
      console.log("++++++++efdwefw++++++++", requestData)
      // this.api.postRequest(api_url, requestData).then(
      //   (res) => {
      //     this.api.setUserLoggedIn(res.result.accessToken, res.result)
      //      //this.router.navigate(['/admin/dashboard'])
      //      window.location.reload();
      //   },
      //   (err) => {

      //   }
      // );
    }
  }


}
