import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiCallingService } from "../../../configs/api-calling.service"

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  public title = 'User Detail';
  public responseData: any = [];
  public id: string = '';
  public IMAGE_BASE_URL;
  public restaurants: any = [];
  public opening_hour: any = [];
  public address_comp: any =[];
  public show= false;
  
  constructor(
    private http: HttpClient,
    private router: Router,
    public api: ApiCallingService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      this.getRestaurants();
    })
    this.api.onActivateScroll();
    this.IMAGE_BASE_URL = this.api.IMAGE_BASE_URL;
  }
  getRestaurants() {
    
    // stop here if form is invalid 

    let api_url = 'Restaurants/GetDetails?access_token='+localStorage.getItem('token');
    api_url = api_url + '&placeId=' + this.id;
    this.api.getRequest(api_url, { placeId: this.id }).then(
      (res) => {
          this.responseData = res;
          this.opening_hour = [];
          this.address_comp = [];
          if(res && res.opening_hours && res.opening_hours.weekday_text.length>0){
            for (var i = 0; i < res.opening_hours.weekday_text.length; i++) {
              this.opening_hour.push( res.opening_hours.weekday_text[i]);
            }
            this.show= true;
          }else{
            this.opening_hour=[];
            this.show= false;
          }

          if(res.rating){
            //const ratings = res.rating;
            const starTotal = 5;
            //const starPercentage = (res.rating / starTotal) * 100;
            res.starPercentageRounded = (res.rating / starTotal) * 100 + `%`;//`${(Math.round(starPercentage / 10) * 10)}%`;

          }else{
            res.starPercentageRounded =  '0%';
          }

          if(res.reviews){
            res.reviewcount = res.reviews.length
          }else{
            res.reviewcount =  '0';
          }
          for (var i = 0; i < res.address_components.length-1; i++) {

            this.address_comp.push(res.address_components[i].long_name);

          }
          //console.log("+++", this.responseData)

      },
      (err) => {

      }
    );

  }
}
