import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NeighbourhoodRestaurantsComponent } from './neighbourhood-restaurants.component';

describe('NeighbourhoodRestaurantsComponent', () => {
  let component: NeighbourhoodRestaurantsComponent;
  let fixture: ComponentFixture<NeighbourhoodRestaurantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NeighbourhoodRestaurantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NeighbourhoodRestaurantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
