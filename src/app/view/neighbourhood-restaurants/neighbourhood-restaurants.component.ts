import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { TabsetComponent, TabDirective } from 'ngx-bootstrap/tabs';
import { LoginComponent } from '../login/login.component';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { NotifierService } from "angular-notifier";
import * as $ from 'jquery';
import { ApiCallingService } from "../../configs/api-calling.service"
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-neighbourhood-restaurants',
  templateUrl: './neighbourhood-restaurants.component.html',
  styleUrls: ['./neighbourhood-restaurants.component.css']
})
export class NeighbourhoodRestaurantsComponent implements OnInit {
  closeResult: string;
  private readonly notifier: NotifierService;
  public restaurants: any = [];
  public userDetail: any = {};
  public limit =100;
  public IMAGE_BASE_URL;
  modalReference: NgbModalRef;
  
  // pagination var
  itemsPerPage: number = 10;
  totalRecords: number;
  skip: number = 0;
  p: number = 1;
  private stage  = '';
  total ='';

  constructor(
    private modalService: NgbModal,
    notifierService: NotifierService,
    public api: ApiCallingService,
    private router: Router,

    ) {  
      this.notifier = notifierService;
      this.userDetail = JSON.parse(localStorage.getItem('userDetails'));
    }

  ngOnInit() {
    this.getRestaturant();
    this.IMAGE_BASE_URL = this.api.IMAGE_BASE_URL;
  }

  getRestaturant(){

    //var api_url =  'Restaurants/NewsFeed?&access_token='+localStorage.getItem('token')+'&location='+localStorage.getItem('latlang')+'&limit='+this.limit+'&codePays='+localStorage.getItem('localcodePays');
    let api_url = 'Restaurants/search?access_token=' + localStorage.getItem('token')+'&offset=' + this.skip + '&limit=' + this.itemsPerPage
   
      api_url = api_url + '&query=' + localStorage.getItem('localcodePays') + '&location='+ this.stage;
    this.api.getRequest(api_url, {}).then( 
      (res) => {
          for(var i in res){
            if(res[i].rating){
           
              res[i].starPercentageRounded =  (res[i].rating / 5) * 100+`%`; //`${(Math.round(starPercentage / 10) * 10)}%`;
            }else{
              res[i].starPercentageRounded =  '0%';
            }
            if(res[i].reviews){
              res[i].reviewcount = res[i].reviews.length

            }else{
              res[i].reviewcount =  '0';
            }
            if(res[i].photo){
              res[i].picToShow = this.IMAGE_BASE_URL+"/Containers/grumpeat/download/"+res[i].photo;
            }else{
              res[i].picToShow= '../../../assets/img/no-photo.png';
            }
            this.restaurants.push(res[i]);
          }
      },
      (err) => {
      }
    );


  }

}
