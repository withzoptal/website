import { Component, OnInit,Input } from '@angular/core';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { SignupComponent } from '../signup/signup.component';
import { ChangePasswordComponent } from '../change-password/change-password.component';
import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiCallingService } from "../../configs/api-calling.service"
import { NotifierService } from "angular-notifier";
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angular-6-social-login';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @Input() name;
  loginForm: FormGroup;
  submitted = false;
  requestData: any;
  private readonly notifier: NotifierService;
  isRemberMeChecked: boolean=false;
 

  constructor(
    public activeModal: NgbActiveModal, 
    private modalService: NgbModal,
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private router: Router,
    public api: ApiCallingService,
    notifierService: NotifierService,
    private socialAuthService: AuthService,
    ) { this.notifier = notifierService;}

  ngOnInit() {

    this.loginForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.compose([
          Validators.required,Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ])]],
        password: ['', [Validators.required, Validators.minLength(4)]],
        rememberMe: [null]
    });
  }
  get f() { return this.loginForm.controls; }

  login() {
    this.submitted = true;

    // stop here if form is invalid 
    if (this.loginForm.invalid) {
      return;
    } else {

      let api_url = 'Profils/login?include=user'

      let requestData = {
        "email": this.loginForm.controls.email.value,
        "password": this.loginForm.controls.password.value,
      }
     
      this.api.postRequest(api_url, requestData).then(
        (res) => { 
           this.notifier.notify('success', `Success: You have logged in successfully .`);  
           this.api.setUserLoggedIn(res.id, res)
           this.router.navigate([this.router.url])
           window.location.reload();
        },
        (err) => {

        }
      );
    }
  }
  public socialSignIn(socialPlatform : string) {
    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }
    // else if(socialPlatform == "google"){
    //   socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    // } 
    // else if (socialPlatform == "linkedin") {
    //   socialPlatformProvider = LinkedinLoginProvider.PROVIDER_ID;
    // }
    
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => { console.log("+++++++++++++Asaaaaaaaaaaa+++++++++++", userData)
        // Now sign-in with userData
        let api_url = 'Profils/login/facebook'
        let requestData = {
          "accessToken": userData.token,
        }
      
        this.api.postRequest(api_url, requestData).then(
          (res) => {  
            this.notifier.notify('success', `Success: You have logged in successfully .`);  
            this.api.setUserLoggedIn(res.id, res)
            //this.router.navigate([this.router.url])
            //window.location.reload();
          },
          (err) => {

          }
        );
      }
    );
  }
  openSignUp(){
    this.modalService.dismissAll(LoginComponent);
    this.modalService.open(SignupComponent);
  }
  openForGot(){
    this.modalService.dismissAll(LoginComponent);
    this.modalService.open(ForgotPasswordComponent);
  }

  onCheckChange(e){
    
     if ( e.target.checked ) {
        this.isRemberMeChecked = true;
      }else{
        this.isRemberMeChecked = false;
      }
  }

}