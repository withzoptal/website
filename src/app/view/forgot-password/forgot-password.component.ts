import { Component, OnInit, Input } from '@angular/core';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { NotifierService } from "angular-notifier";
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiCallingService } from "../../configs/api-calling.service"
import Swal from 'sweetalert2/dist/sweetalert2.js'



@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  @Input() name;
  forgotPassword: FormGroup;
  submitted = false;
  requestData: any;
  private readonly notifier: NotifierService;
  public userDetail: any = {};
  
  
  constructor(
    public activeModal: NgbActiveModal, 
    private modalService: NgbModal,
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private router: Router,
    public api: ApiCallingService,
    notifierService: NotifierService,  
    ) { 
      this.notifier = notifierService;
      this.userDetail = JSON.parse(localStorage.getItem('userDetails'));
    }

  ngOnInit() {
    this.forgotPassword = this.formBuilder.group({
      email: ['', [
                Validators.required, 
                Validators.compose([
                    Validators.required,Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
                ])]
              ],
       });

  }
  get f() { return this.forgotPassword.controls; }

  sendLink(){

    this.submitted = true;
    // stop here if form is invalid 
    if (this.forgotPassword.invalid) {
      return;
    } else {

      let api_url = 'Profils/reset';
      
      let requestData = {
        "email": this.forgotPassword.controls.email.value,
      }

      this.api.postRequest(api_url,requestData).then( 
        (res) => {  this.modalService.dismissAll(ForgotPasswordComponent);
           Swal.fire(
              'Forgot password!',
              'Email sent successfully.',
              'success'
            )

            this.router.navigate(['/home']);
        },
        (err) => {
          Swal.fire(
            'Forgot password!!',
            'Please provide valid registered.',
            'error'
          )
        }
      );


    }

  }


}
