import { Component, OnInit } from '@angular/core';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiCallingService } from "../../configs/api-calling.service"
import { NotifierService } from "angular-notifier";
import { FavoriteRestaurantsComponent } from '../favorite-restaurants/favorite-restaurants.component';

import * as $ from 'jquery';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent implements OnInit {
  
  private readonly notifier: NotifierService;
  public userDetail: any = {};
  deleteId ='';

  constructor(
    public activeModal: NgbActiveModal,
    private modalService: NgbModal,
    private http: HttpClient,
    private router: Router,
    public api: ApiCallingService,
    notifierService: NotifierService,
    //public favservice: FavoriteRestaurantsComponent,
    ) { 
      this.notifier = notifierService;
      this.userDetail = JSON.parse(localStorage.getItem('userDetails'));
    }

  ngOnInit() {
  }

  deleteFav(deleteId){
     var isDelete =  $('.SeeMore2').hasClass('SeeMore2') ? false : true;
    
      if(isDelete == true){
          var api_url='Restaurants/deleteFavRest?access_token='+localStorage.getItem('token');
          this.api.postRequest(api_url, {restaurantId: deleteId }).then( 
            (res) => {
              this.notifier.notify('success', `Success: Restaurant deleted successfully.`)
              this.modalService.dismissAll(ConfirmComponent);
              this.router.navigate(['favorite-restaurants']);
              //this.favservice.getFavorite();
            },
            (err) => {
            }
          );
      }

  }

}
