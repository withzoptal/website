import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from '../login/login.component';
import { SignupComponent } from '../signup/signup.component';
import { ChangePasswordComponent } from '../change-password/change-password.component';
import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiCallingService } from "../../configs/api-calling.service"
import { NotifierService } from "angular-notifier";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  private readonly notifier: NotifierService;
  userToken ="";
  private loginUser={};
  show: boolean = true;
  account: boolean = true;
  constructor(
    private modalService: NgbModal,
    private http: HttpClient,
    private router: Router,
    public api: ApiCallingService,
    notifierService: NotifierService,
    ) { this.notifier = notifierService; }

  ngOnInit() {
    
    this.userToken = localStorage.getItem('token');
    this.loginUser = JSON.parse(localStorage.getItem('userDetails'));
  
    if(this.userToken == null || this.userToken == '' ){
    this.show = true;
    this.account = false;

    }else{
      this.show = false;
      this.account = true;
    } 

  }
  openLogin() {
    this.modalService.open(LoginComponent);
  }
  openSignUp(){
    this.modalService.dismissAll(LoginComponent);
    this.modalService.open(SignupComponent);
  }
  openForGot(){
    this.modalService.dismissAll(LoginComponent);
    this.modalService.open(ForgotPasswordComponent);
  }

  logout() {
    var api_url = 'Profils/logout?access_token='+localStorage.getItem('token')
    this.api.postRequest(api_url, {access_token: localStorage.getItem('token')}).then( 
      (res) => {
        this.router.navigate(['/home']);
        if (res && res == true || res.code == 200 ) {
          localStorage.clear();
          window.location.reload();
        } else {

        }
      },
      (err) => {
      }
    );

  }


}
