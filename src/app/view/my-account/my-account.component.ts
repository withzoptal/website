import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { TabsetComponent, TabDirective } from 'ngx-bootstrap/tabs';
import { NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiCallingService } from "../../configs/api-calling.service"
import { NotifierService } from "angular-notifier";
import * as $ from 'jquery';
import Swal from 'sweetalert2/dist/sweetalert2.js'

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css']
})
export class MyAccountComponent implements OnInit {
  disableSwitching: boolean;
  @ViewChild('tabset', {read: ElementRef}) tabsetEl: ElementRef;
  private readonly notifier: NotifierService;
  public following: any = [];
  public followers: any = [];
  public favorites: any = [];
  public countries: any =[];
  public fav_restaurants: any = [];
  public userDetail: any = {};
  public tiers = '';
  public IMAGE_BASE_URL;
  public limit= 1000;
  public isShow = false;
  activeTab = "$";
  public profile ="";
  public avatar ="";
  public about ="";

  constructor(
    private modalService: NgbModal,
    private http: HttpClient,
    private router: Router,
    public api: ApiCallingService,
    notifierService: NotifierService,
    
    ) { 
      this.notifier = notifierService;
      this.userDetail = JSON.parse(localStorage.getItem('userDetails'));
    }

  ngOnInit() {

    this.getFollowing();
    this.getFollowers();
    this.getCountry();
    this.getFavorite();
    this.getProfile();
    
    this.IMAGE_BASE_URL = this.api.IMAGE_BASE_URL;
    $(function() {
      $('.SeeMore2').click(function(){ 
        var $this = $(this);
        $this.toggleClass('SeeMore2');
        if($this.hasClass('SeeMore2')){
      
          $this.html('<i class="icofont-pencil-alt-5"></i>');		
          $(".favDiv").removeClass('fa fa-trash-o').addClass('icofont-heart-alt')	
          $(".dollarPanel").addClass('pencilActive');

        } else {
          
          $this.html('<i class="fa fa-remove"></i>');
          $(".favDiv").removeClass('icofont-heart-alt').addClass('fa fa-trash-o')
          $(".dollarPanel").addClass('removeActive');

        }
      });
    });
    this.api.onActivateScroll();
  }
  
  getProfile(){


    var api_url = 'Profils/'+this.userDetail.userId+'?access_token='+localStorage.getItem('token')
    this.api.getRequest(api_url, {}).then( 
      (res) => {
          this.profile = res;

          this.avatar = res.avatar ? res.avatar :  this.userDetail.user.avatar;
          this.about = res.about ? res.about :  this.userDetail.user.about;

          
        console.log("+++++++++++++++++++++",  this.profile)

      },
      (err) => {
      }
    );
  }



  deleteFav(restId){
    
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete this restaurants form favourite!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
       
        var api_url='Restaurants/deleteFavRest?access_token='+localStorage.getItem('token');
        this.api.postRequest(api_url, {restaurantId: restId }).then( 
          (res) => {
             Swal.fire(
                'Deleted!',
                'Restaurants has been deleted form favourite list.',
                'success'
              )
            this.getFavorite();
          },
          (err) => {
          }
        );
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Restaurants is still in favourite list :)',
          'error'
        )
      }
    })
  }

  toggleDisplay(input){
    // this.isShow ? $("#restoSelect").show() : $("#restoSelect").hide() ;
    this.isShow = !this.isShow;
    // $(".searchrestaurant").append('<input type="text" name="addResto" class="form-control" id="addResto" placeholder="Search restaurants" data-rule="minlen:4" data-msg="Please enter at least 4 chars">');
  }

  showActiveTabId() {
    let tabId;
    tabId = this.activeTab; 
    
    let doller = tabId == 'dollarone' ? "1" : (tabId == 'dollartwo' ? "2" : (tabId == 'dollarthree' ? "3" : (tabId == 'dollarfour' ? "4" : "")));
    //$(".cost").html(doller);
    $(".dollarPanel").attr('id', doller);
    this.getFavorite(doller);

  }

  getFollowing(){
   
    var api_url = 'Follows/listFollowing?access_token='+localStorage.getItem('token')+'&userId='+this.userDetail.userId
    this.api.getRequest(api_url, {}).then( 
      (res) => {
          this.following = res.list;
      },
      (err) => {
      }
    );

  }
  getFollowers(){
   
    var api_url = 'Follows/listFollowers?access_token='+localStorage.getItem('token')+'&userId='+this.userDetail.userId
    this.api.getRequest(api_url, {}).then( 
      (res) => {
          this.followers = res.list;
      },
      (err) => {
      }
    );

  }

  getFavorite(tiers =null){
    let x = tiers!= null ? tiers : 1 ;
    
    var api_url = 'Restaurants/favoriteRestaurant?access_token='+localStorage.getItem('token')+'&userId='+this.userDetail.userId
    this.api.getRequest(api_url, {}).then( 
      (res) => {

          this.favorites = JSON.parse(JSON.stringify(res.list))
          this.fav_restaurants = [];
          this.countries=[];
          var counti = $("#countryIS").val();
          var check_to_delete =  $('.SeeMore2').hasClass('SeeMore2') ? false : true;
          for(var i in this.favorites){
            if(this.favorites[i].restaurant.location.country == counti){
              if(this.favorites[i].restaurant.price.tier == x){ 
                if(this.favorites[i].restaurant && this.favorites[i].restaurant.photos && this.favorites[i].restaurant.photos.length > 0){
                  //this.favorites[i].picToShow=this.favorites[i].restaurant.photos[0].photo_reference;
                  // this.favorites[i].picToShow ="https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="+this.favorites[i].restaurant.photos[0].photo_reference+"&sensor=false&key=AIzaSyDe06nYV9PKC3LHUOhqNADqtO1lIlcoaC0";
                  this.favorites[i].picToShow = this.IMAGE_BASE_URL+"/Containers/grumpeat/download/"+this.favorites[i].restaurant.photo;
                }else{
                  this.favorites[i].picToShow= '../../../assets/img/no-photo.png';
                }
                if(this.favorites[i].restaurant.rating){
                  const ratings = this.favorites[i].restaurant.rating;
                  const starTotal = 5;
                  const starPercentage = (ratings / starTotal) * 100;
                  this.favorites[i].starPercentageRounded =starPercentage+ `%`; `${(Math.round(starPercentage / 10) * 10)}%`;
                }else{
                  this.favorites[i].starPercentageRounded =  '0%';
                }
                if(this.favorites[i].restaurant.reviews){
                  this.favorites[i].reviewcount = this.favorites[i].restaurant.reviews.length

                }else{
                  this.favorites[i].reviewcount =  '0';
                }
                if(check_to_delete == true){
                  this.favorites[i].deleteFav = true;

                }else{
                  this.favorites[i].deleteFav = false;

                }
                this.fav_restaurants.push(this.favorites[i]);
              }
            }
            this.countries.push(this.favorites[i].restaurant.location.country);
            this.countries=  this.remove_duplicates(this.countries);

          }
      },
      (err) => {
      }
    );

  }

  remove_duplicates(arr) {
    let obj = {};
    for (let i = 0; i < arr.length; i++) {
        obj[arr[i]] = true;
    }
    arr = [];
    for (let key in obj) {
        arr.push(key);
    }
    return arr;
}

  unFollowing(profileId){ 
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to unfollow!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, unfollow it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        var api_url ="Follows/unfollowUser?access_token="+localStorage.getItem('token')+'&profilId='+profileId
        this.api.deleteRequest(api_url, {profilId: profileId }).then( 
          (res) => {
             Swal.fire(
                'Unfollow!',
                'You have successfully unfollowed.',
                'success'
              )
            this.getFollowing();
          },
          (err) => {
          }
        );
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'User is still in following list :)',
          'error'
        )
      }
    })


  } 

  unfollow(profileId){
      Swal.fire({
        title: 'Are you sure?',
        text: 'You want to unfollow!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, unfollow it!',
        cancelButtonText: 'No, keep it'
      }).then((result) => {
        if (result.value) {
          var api_url ="Follows/unfollowUser?access_token="+localStorage.getItem('token')+'&profilId='+profileId
          this.api.deleteRequest(api_url, {profilId: profileId }).then( 
            (res) => {
               Swal.fire(
                  'Unfollow!',
                  'You have successfully unfollowed.',
                  'success'
                )
              this.getFollowers();
            },
            (err) => {
            }
          );
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire(
            'Cancelled',
            'User is still in following list :)',
            'error'
          )
        }
      })
    

  }
  follow(profileId){

    var api_url ="Follows/followUser?access_token="+localStorage.getItem('token')
    this.api.postRequest(api_url, {profilId: profileId }).then( 
      (res) => {
          Swal.fire(
            'follow!',
            'You have successfully followed.',
            'success'
          )
        this.getFollowers();
      },
      (err) => {
      }
    );
       
  }

  selectOption(value){
    $("#countryIS").val(value)
    let tiers = $(".dollarPanel").attr('id')
    this.getFavorite(tiers);
  }

  getCountry(){
   
    var api_url = 'Restaurants/favoriteRestaurant?access_token='+localStorage.getItem('token')+'&userId='+this.userDetail.userId
    this.api.getRequest(api_url, {}).then( 
      (res) => {

          this.favorites = JSON.parse(JSON.stringify(res.list))
          this.countries=[];
        
          for(var i in this.favorites){
            this.countries.push(this.favorites[i].restaurant.location.country);
            this.countries=  this.remove_duplicates(this.countries);
          }

          $("#countryIS").val(this.countries[0])
      },
      (err) => {
      }
    );

  }



}
