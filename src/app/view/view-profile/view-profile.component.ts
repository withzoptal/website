import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { TabsetComponent, TabDirective } from 'ngx-bootstrap/tabs';
import { NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiCallingService } from "../../configs/api-calling.service"
import { NotifierService } from "angular-notifier";
import * as $ from 'jquery';
import Swal from 'sweetalert2/dist/sweetalert2.js'

@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.css']
})
export class ViewProfileComponent implements OnInit {
  disableSwitching: boolean;
  @ViewChild('tabset', {read: ElementRef}) tabsetEl: ElementRef;
  private readonly notifier: NotifierService;
  public following: any = [];
  public followers: any = [];
  public favorites: any = [];
  public countries: any =[];
  public country: any =[];
  public fav_restaurants: any = [];
  public userDetail: any = {};
  public visitorDetail: any = {};
  public tiers = '';
  public IMAGE_BASE_URL;
  public limit= 50;
  public isShow = false;
  public id: string = '';
  activeTab = "$";
  
  constructor(
    private modalService: NgbModal,
    private http: HttpClient,
    private router: Router,
    public api: ApiCallingService,
    notifierService: NotifierService,
    private activatedRoute: ActivatedRoute

    ) { 
      this.notifier = notifierService;
      this.userDetail = JSON.parse(localStorage.getItem('userDetails'));
      this.IMAGE_BASE_URL = this.api.IMAGE_BASE_URL;
    }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    })
    this.vistorProfile(this.id);
    this.visitorFollowers(this.id);
    this.visitorFollowing(this.id);
    this.countryDrop();
    this.getFavorite()
  }

  showActiveTabId() {
    let tabId;
    tabId = this.activeTab; 
    
    let doller = tabId == 'dollarone' ? "1" : (tabId == 'dollartwo' ? "2" : (tabId == 'dollarthree' ? "3" : (tabId == 'dollarfour' ? "4" : "")));
    //$(".cost").html(doller);
    $(".dollarPanel").attr('id', doller);
    this.getFavorite(doller);

  }

  toggleDisplay(input){
    // this.isShow ? $("#restoSelect").show() : $("#restoSelect").hide() ;
    this.isShow = !this.isShow;
    // $(".searchrestaurant").append('<input type="text" name="addResto" class="form-control" id="addResto" placeholder="Search restaurants" data-rule="minlen:4" data-msg="Please enter at least 4 chars">');

  }


  vistorProfile(profileId){
      
      var api_url = 'Profils/'+profileId+'?access_token='+localStorage.getItem('token');
      this.api.getRequest(api_url, {}).then( 
        (res) => { 
           this.visitorDetail =  res;
        },
        (err) => {
        }
      );

  }

  visitorFollowers(profileId){
  //Follows/listFollowers?userId=5cf89872ccc452144e386282&limit=25&access_token=v6WVcTSQwARHcYZG0KoOG9hvtYjifHRAD2AdiDgezDDEgxjwngSHGzpQwdDWNDgW
    var api_url = 'Follows/listFollowers?userId='+profileId+'&access_token='+localStorage.getItem('token') +'&limit='+this.limit
    console.log("++++",api_url)
    this.api.getRequest(api_url, {}).then( 
      (res) => {
          this.followers = res.list;
      },
      (err) => {
      }
    );

  }
  visitorFollowing(profileId){
   
    var api_url = 'Follows/listFollowing?access_token='+localStorage.getItem('token')+'&userId='+profileId  +'&limit='+this.limit
    
    this.api.getRequest(api_url, {}).then( 
      (res) => {
          this.following = res.list;
      },
      (err) => {
      }
    );
  }

  selectOption(value){
    $("#countryIS").val(value)
    let tiers = $(".dollarPanel").attr('id')
    this.getFavorite(tiers);
  }

  countryDrop(){

    var api_url = 'Restaurants/favoriteRestaurant?access_token='+localStorage.getItem('token')+'&userId='+this.id
   
    this.api.getRequest(api_url, {}).then( 
      (res) => {
          this.favorites = JSON.parse(JSON.stringify(res.list));
          this.country= [];
          for(var i in this.favorites){
            this.country.push(this.favorites[i].restaurant.location.country);
          }
          this.country=  this.remove_duplicates(this.country);
          $("#countryIS").val(this.country[0]) 

      },
      (err) => {
      }
    );

  }

  getFavorite(tiers =null){

    let x = tiers!= null ? tiers : 4 ; 
    var api_url = 'Restaurants/favoriteRestaurant?access_token='+localStorage.getItem('token')+'&userId='+this.id
   
    this.api.getRequest(api_url, {}).then( 
      (res) => {

          this.favorites = JSON.parse(JSON.stringify(res.list)); 
          this.fav_restaurants = [];
          this.countries=[];
          var check_to_delete =  $('.SeeMore2').hasClass('SeeMore2') ? false : true;
          var counti = $("#countryIS").val();
         
          for(var i in this.favorites){
            if(this.favorites[i].restaurant.location.country == counti){
              if(this.favorites[i].restaurant.price.tier == x){ 
                if(this.favorites[i].restaurant && this.favorites[i].restaurant.photos && this.favorites[i].restaurant.photos.length > 0){
                  this.favorites[i].picToShow = this.IMAGE_BASE_URL+"Containers/grumpeat/download/"+this.favorites[i].restaurant.photo;
                }else{
                  this.favorites[i].picToShow= '../../../assets/img/no-photo.png';
                }
                if(this.favorites[i].restaurant.rating){
                  const ratings = this.favorites[i].restaurant.rating;
                  const starTotal = 5;
                  const starPercentage = (ratings / starTotal) * 100;
                  this.favorites[i].starPercentageRounded = `${(Math.round(starPercentage / 10) * 10)}%`;
                }else{
                  this.favorites[i].starPercentageRounded =  '0%';
                }
                if(this.favorites[i].restaurant.reviews){
                  this.favorites[i].reviewcount = this.favorites[i].restaurant.reviews.length

                }else{
                  this.favorites[i].reviewcount =  '0';
                }
                if(check_to_delete == true){
                  this.favorites[i].deleteFav = true;

                }else{
                  
                  this.favorites[i].deleteFav = false;

                }
                this.fav_restaurants.push(this.favorites[i]);
              }
            }
            this.countries.push(this.favorites[i].restaurant.location.country);
            this.countries=  this.remove_duplicates(this.countries);
          }
      },
      (err) => {
      }
    );

  }

  remove_duplicates(arr) {
    let obj = {};
    for (let i = 0; i < arr.length; i++) {
        obj[arr[i]] = true;
    }
    arr = [];
    for (let key in obj) {
        arr.push(key);
    }
    return arr;
  }

  refresh(): void {
    window.location.reload();
  }

  deleteFav(){

     console.log("delete");

  }

}
