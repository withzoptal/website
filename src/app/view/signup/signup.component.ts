import { Component, OnInit } from '@angular/core';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiCallingService } from "../../configs/api-calling.service"
import { NotifierService } from "angular-notifier";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signUpForm: FormGroup;
  submitted = false;
  requestData: any;
  fileToUpload: File = null;
  public fileUrl: any = '';
  private readonly notifier: NotifierService;

  constructor(
    public activeModal: NgbActiveModal, 
    private modalService: NgbModal,
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private router: Router,
    public api: ApiCallingService,
    notifierService: NotifierService
    ) { this.notifier = notifierService; }

  ngOnInit() {
    this.signUpForm = this.formBuilder.group({
      username: ['krishan@124', [Validators.required, Validators.minLength(2)]],
      firstname: ['krishan', [Validators.required, Validators.minLength(2)]],
      lastname: ['kumar', [Validators.required, Validators.minLength(2)]],
      email: ['krishan.zoptal@gmail.com', [Validators.required, Validators.compose([
          Validators.required, 
          Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$")
        ])
      ]],
      password: ['12345678', [Validators.required, Validators.minLength(4)]],
      avatar: [null],
    });
  }

  get f() { return this.signUpForm.controls; }
  signup(){
    this.submitted = true;
    // stop here if form is invalid 
    if (this.signUpForm.invalid) { 
      return;
    } else {
      let api_url = 'Profils/specialCreate';
      let requestData = {
          "username": this.signUpForm.controls.username.value,
          "firstName": this.signUpForm.controls.firstname.value,
          "lastName": this.signUpForm.controls.lastname.value,
          "email": this.signUpForm.controls.email.value,
          "password": this.signUpForm.controls.password.value,
          //"avatar": this.fileToUpload == null ? '' : this.fileToUpload,
        }


      this.api.postRequest(api_url, requestData).then(
        (res) => {  
            if(res.id){
                this.api.setUserLoggedIn(res.id, res)
                this.notifier.notify('success', `Success: Your account has been created successfully.`);
                this.modalService.dismissAll(SignupComponent);
                window.location.reload();
                this.router.navigate([this.router.url])
            } else{

              this.notifier.notify('error', `Failed: Somthing went wrong please try again.`);

            }
        },
        (err) => {

        }
      ); 

    }
   
  }
  
  handleFileInput(files: FileList) {
    
    var mimeType = files.item(0).type;
    // if (mimeType.match(/image\/*/) == null) {
    // this.file_warning= "Only images are supported.";
    // return;
    // }
    var reader = new FileReader();
    // var img = new Image();
    // this.imagePath = event.target.files;
    reader.readAsDataURL(files.item(0));
    reader.onload = (_event) => {
      this.fileUrl = reader.result;
    }
    this.fileToUpload = files.item(0);
  }


}
