import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { HttpClient } from '@angular/common/http';
import { ApiCallingService } from "../../configs/api-calling.service"
import { NotifierService } from "angular-notifier";
import Swal from 'sweetalert2/dist/sweetalert2.js'

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  editForm: FormGroup;
  submitted = false;
  requestData: any;
  public userDetail: any = {};
  fileToUpload: File = null;
  public fileUrl: any = '';
  imgData : any;
  private readonly notifier: NotifierService;
  public GOOGLE_API_KEY;
  public result = {};


  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private router: Router,
    public api: ApiCallingService,
    private spinner: NgxSpinnerService,
    notifierService: NotifierService
  ) { 
    this.notifier = notifierService;
    this.userDetail = JSON.parse(localStorage.getItem('userDetails')); 
  }

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(2)]],
      firstName: ['', [Validators.required, Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.compose([
          Validators.required, 
          Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$")
        ])
      ]],
      avatar: [null],
      about: ['', [Validators.required, Validators.minLength(10)]],
    });
    this.GOOGLE_API_KEY = this.api.GOOGLE_API_KEY;
    this.api.onActivateScroll();
    this.getProfile();
  }
  
  getProfile(){
  
    let api_url = 'Profils/'+this.userDetail.user.id+'?access_token='+localStorage.getItem('token');
    
    this.api.patchRequest(api_url,{}).then( 
      (res) => {
      
        this.editForm = this.formBuilder.group({
          username: [res.username, [Validators.required, Validators.minLength(2)]],
          firstName: [res.firstName, [Validators.required, Validators.minLength(2)]],
          lastName: [res.lastName, [Validators.required, Validators.minLength(2)]],
          email: [res.email, [Validators.required, Validators.compose([
              Validators.required, 
              Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$")
            ])
          ]],
          avatar: [null],
          about: [res.about, [Validators.required, Validators.minLength(10)]],
        });

      },
      (err) => {
        
      }
    );

  }

  get f() { return this.editForm.controls; }
  update(){
    this.submitted = true;
    // stop here if form is invalid 
    if (this.editForm.invalid) { 
      return;
    } else {
     
      let api_url = 'Profils/update?where=%7B%22id%22%20%3A%20%22'+this.userDetail.user.id+'%22%7D&access_token='+localStorage.getItem('token')
      let requestData = {
          "username": this.editForm.controls.username.value,
          "firstName": this.editForm.controls.firstName.value,
          "lastName": this.editForm.controls.lastName.value,
          "email": this.editForm.controls.email.value,
          "about": this.editForm.controls.about.value,
          "avatar": this.fileToUpload,
        }
      this.api.postRequest(api_url, requestData).then( 
        (res) => { 
          
        if(res.count == 1){
          Swal.fire(
                'Profile!',
                'Your profile information updated successfully.',
                'success'
              )
              //this.getProfile();
              this.router.navigate(['/my-account']);
        }else{
          Swal.fire(
            'Profile!',
            'Somthing went wrong please try again!.',
            'error'
          )

        }
        },
        (err) => {
          Swal.fire(
            'Profile!',
            'Please provide valid data.',
            'error'
          )
        }
      );
      }
    }

    handleFileInput(files: FileList) {
      var mimeType = files.item(0).type;
      // if (mimeType.match(/image\/*/) == null) {
      // this.file_warning= "Only images are supported.";
      // return;
      // }
      var reader = new FileReader();
      // var img = new Image();
      // this.imagePath = event.target.files;
      reader.readAsDataURL(files.item(0));
      reader.onload = (_event) => {
        this.fileUrl = reader.result;
      }
  
    const formData = new FormData();
    formData.append('avatar', files.item(0));

    this.http.post('http://3.0.175.106/api/Containers/grumpeat/upload?access_Token='+localStorage.getItem('token'), formData).subscribe(
      
    data => {
      this.fileToUpload =  data['result']['files']['avatar'][0]['name'];

    },
    (err) => { 
       this.notifier.notify('error', ' WHOOPS , ' + 'Please try again with other image less then 10 KB');
      }  //console.log("+++++++++++++", err.statusText)
    )
  }



}
